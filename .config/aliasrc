#!/bin/sh
# Use neovim for vim if present.
[ -x "$(command -v nvim)" ] && alias vim="nvim" vimdiff="nvim -d"

# Replace docker with podman if podman is installed
[ -x "$(command -v podman)" ] && alias docker="podman"

# Verbosity and settings that you pretty much just always are going to want.
alias \
	cp="cp -iv" \
	mv="mv -iv" \
	rm="rm -vI" \
	mkd="mkdir -pv" \
	yt="youtube-dl --add-metadata -i" \
	yta="yt -x -f bestaudio/best" \
	ffmpeg="ffmpeg -hide_banner"

# Colorize commands when possible.
alias \
	grep="grep --color=auto" \
	diff="diff --color=auto" \
	ccat="highlight --out-format=ansi"

# These common commands are just too long! Abbreviate them.
alias \
	ka="killall" \
	g="git" \
	sdn="sudo shutdown -h now" \
	f="$FILE" \
	e="$EDITOR" \
	v="$EDITOR" \
	c="code" \
	cde="code ." \
	zt="zathura"

alias \
	magit="nvim -c MagitOnly" \
	tmux="tmux -f ${XDG_CONFIG_HOME:-$HOME/.config}/tmux/tmux.conf" \
	btm="btm -g --battery" \
	cnp="commitandpush" \
	sp="speedtest" \
	pop_os_up="topgrade ; agar -y" \
	ubuntu_up="topgrade ; sudo ubuntu-drivers install" \
	rr="SHELL=$HOME/.local/bin/r.shell ranger" \
	vs="v -O" \
	sp="v -o" \
	vwg="vimwiki-git"
