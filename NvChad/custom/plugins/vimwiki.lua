local g = vim.g

g.vimwiki_global_ext = 0
g.vimwiki_table_mappings = 0

vim.cmd [[ let g:vimwiki_list = [{'auto_diary_index': 1}] ]]
