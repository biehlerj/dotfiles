# Jacob Biehler's Dotfiles

## Table of Contents

- [Jacob Biehler's Dotfiles](#jacob-biehlers-dotfiles)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [How to install my dotfiles](#how-to-install-my-dotfiles)

## Introduction

This repository contains my dotfiles managed with [yadm](https://yadm.io/). My dotfiles have been tested the most on my laptop running Pop!_OS 21.04, but do work on Arch and Artix as well.

## How to install my dotfiles

Make sure yadm is installed and run `yadm clone --bootstrap https://gitlab.com/biehlerj/dotfiles.git`
